<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="pt-24 font-sans text-grey-darkest md:bg-grey-lighter">
    <div id="app" class="h-full flex flex-col overflow-hidden">
        <div class="border-b bg-white h-12">
            <div class="md:container mx-auto flex justify-between items-center h-12 px-4">
                <a class="no-underline text-grey-darkest" href="{{ url('/') }}">
                    <span class="hidden md:inline">{{ config('app.name') }}</span>
                    <span class="md:hidden">{{ config('app.short_name') }}</span>
                </a>

                <div class="inline-flex">
                    @guest
                        <a class="no-underline text-grey-darkest px-2 block h-full" href="{{ route('login') }}">{{ __('Login') }}</a>
                        <a class="no-underline text-grey-darkest px-2 block h-full" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @else
                        <div class="flex">
                            <a class="hidden md:block no-underline text-grey-darkest pr-4" href="#">
                                {{ Auth::user()->name }}
                            </a>

                            <a class="no-underline text-grey-darkest" href="{{ route('logout') }}"
                                onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    @endguest
                </div>

            </div>
        </div>

        <div class="flex-1 flex flex-col overflow-hidden md:py-4">
            @yield('content')
        </div>
    </div>
</body>
</html>

