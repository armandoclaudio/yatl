@extends('layouts.app')

@section('content')
<div class="container mx-auto w-full max-w-md bg-white p-6 rounded md:shadow-lg md:mt-8">
    <h1 class="text-lg font-semibold mb-6">{{ __('Register') }}</h1>
    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="form-group">
            <label class="form-label" for="name">{{ __('Name') }}</label>
            <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' border-red' : '' }}" name="name" value="{{ old('name') }}" autofocus>

            @if ($errors->has('name'))
                <span class="form-error">
                    <i>{{ $errors->first('name') }}</i>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label class="form-label" for="email">{{ __('E-Mail Address') }}</label>

            <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' border-red' : '' }}" name="email" value="{{ old('email') }}">

            @if ($errors->has('email'))
                <span class="form-error">
                    <i>{{ $errors->first('email') }}</i>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label class="form-label" for="password">{{ __('Password') }}</label>
            <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' border-red' : '' }}" name="password">

            @if ($errors->has('password'))
                <span class="form-error">
                    <i>{{ $errors->first('password') }}</i>
                </span>
            @endif  
        </div>

        <div class="form-group">
            <label class="form-label" for="password-confirm">{{ __('Confirm Password') }}</label>
            <input id="password-confirm" type="password" class="form-control " name="password_confirmation">
        </div>

        <button type="submit" class="btn btn-blue">
            {{ __('Register') }}
        </button>
    </form>
</div>
@endsection
