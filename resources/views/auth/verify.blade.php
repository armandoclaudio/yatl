@extends('layouts.app')

@section('content')
<div class="container mx-auto w-full max-w-md bg-white p-6 rounded md:shadow-lg md:mt-8">
    <h1 class="text-lg font-semibold mb-6">{{ __('Verify Your Email Address') }}</h1>
    @if (session('resent'))
        <p class="mb-4">
            {{ __('A fresh verification link has been sent to your email address.') }}
        </p>
    @endif

    {{ __('Before proceeding, please check your email for a verification link.') }}
    {{ __('If you did not receive the email') }}, <a class="no-underline text-blue hover:underline" href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
</div>
@endsection
