@extends('layouts.app')

@section('content')
<div class="container mx-auto w-full max-w-md bg-white p-6 rounded md:shadow-lg md:mt-8">
    <h1 class="text-lg font-semibold mb-6">{{ __('Login') }}</h1>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group">
            <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' border-red' : '' }}" name="email" value="{{ old('email') }}" autofocus>
            @if ($errors->has('email'))
                <span class="text-red text-xs font-italic">
                    <i>{{ $errors->first('email') }}</i>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="password" class="form-label">{{ __('Password') }}</label>
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' border-red' : '' }}" name="password">

            @if ($errors->has('password'))
                <span class="text-red text-xs font-italic">
                    <i>{{ $errors->first('password') }}</i>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label class="flex items-center">
                <input type="checkbox" name="remember" class="mr-1" {{ old('remember') ? 'checked' : '' }}>
                <span class="flex-1">{{ __('Remember Me') }}</span>
            </label>
        </div>

        <div class="flex items-center">
            <button type="submit" class="btn btn-blue mr-4">
                {{ __('Login') }}
            </button>
            <a class="flex-1 no-underline hover:underline text-blue" href="#">
                {{ __('Forgot Your Password?') }}
            </a>
        </div>
    </form>
</div>
@endsection
