require('./bootstrap');
import router from './routes.js'

Vue.component('todo-list', require('./components/TodoList.vue'));
Vue.component('todo-task', require('./components/TodoTask.vue'));
Vue.component('todo-form', require('./components/TodoForm.vue'));
Vue.component('sortable-list', require('./components/SortableList.vue'));

window.Events = new Vue()
const app = new Vue({
    el: '#app',

    router
})
