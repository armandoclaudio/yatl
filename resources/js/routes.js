import VueRouter from 'vue-router'

let routes = [
    {
        path: '/',
        component: require('./views/Home.vue')
    }
]

export default new VueRouter({
    routes
})