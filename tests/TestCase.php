<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $user;

    public function signIn($user = null)
    {
        if ($user == null) {
            $user = factory('App\User')->create();
        }

        $this->actingAs($user);
        $this->user = $user;

        return $this;
    }

    public function create($class, $overrides = [])
    {
        return factory($class)->create($overrides);
    }
}
