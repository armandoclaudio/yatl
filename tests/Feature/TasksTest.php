<?php

namespace Tests\Feature;

use App\Task;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TasksTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_see_tasks()
    {
        $this->get(route('tasks'))
            ->assertStatus(401);
    }

    /** @test */
    public function a_user_can_their_tasks()
    {
        $this->signIn();

        $task = $this->create('App\Task', [
            'user_id' => $this->user->id,
            'description' => 'Task description',
        ]);

        $this->get(route('tasks'))
            ->assertJsonFragment([
                'id' => $task->id,
                'description' => $task->description,
            ]);
    }

    /** @test */
    public function a_user_cannot_see_other_users_tasks()
    {
        $this->signIn();

        $task1 = $this->create('App\Task', [
            'user_id' => $this->user->id,
            'description' => 'Task description',
        ]);
        $task2 = $this->create('App\Task', [
            'description' => 'Task description',
        ]);

        $this->get(route('tasks'))
            ->assertJsonFragment([
                'id' => $task1->id,
            ])
            ->assertJsonMissing([
                'id' => $task2->id,
            ]);
    }

    /** @test */
    public function a_task_can_be_created()
    {
        $this->signIn();

        $this->post(route('tasks.store'), [
            'description' => 'Task description'
        ]);

        $this->assertDatabaseHas('tasks', [
            'user_id' => $this->user->id,
            'description' => 'Task description',
        ]);
    }

    /** @test */
    public function a_task_can_be_updated()
    {
        $this->signIn();
        $task = $this->create('App\Task', [
            'user_id' => $this->user->id,
            'description' => 'Some description',
        ]);

        $this->patch(route('tasks.update', $task->id), [
            'description' => 'New description'
        ]);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'description' => 'New description',
        ]);
    }

    /** @test */
    public function a_task_can_be_soft_deleted()
    {
        $this->signIn();
        $task = $this->create('App\Task', [
            'user_id' => $this->user->id,
        ]);
        $now = Carbon::now();
        Carbon::setTestNow($now);

        $this->delete(route('tasks.destroy', $task->id));
        
        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'deleted_at' => $now
        ]);
        $this->assertNull(Task::find($task->id));
    }

    /** @test */
    public function a_task_can_be_completed()
    {
        $this->signIn();
        $task = $this->create('App\Task', [
            'user_id' => $this->user->id,
            'completed_at' => null
        ]);

        $this->patch(route('tasks.status.completed', $task->id));

        $this->assertNotNull($task->fresh()->completed_at);
    }

    /** @test */
    public function a_task_can_be_changed_to_incomplete()
    {
        $this->signIn();
        $task = $this->create('App\Task', [
            'user_id' => $this->user->id,
            'completed_at' => Carbon::now(),
        ]);

        $this->patch(route('tasks.status.incomplete', $task->id));

        $this->assertNull($task->fresh()->completed_at);
    }

    /** @test */
    public function a_user_cannot_update_delete_another_users_tasks()
    {
        $task = $this->create('App\Task');
        $this->signIn();

        $this->patch(route('tasks.update', $task->id))
            ->assertStatus(403);

        $this->delete(route('tasks.destroy', $task->id))
            ->assertStatus(403);

        $this->patch(route('tasks.status.completed', $task->id))
            ->assertStatus(403);

        $this->patch(route('tasks.status.incomplete', $task->id))
            ->assertStatus(403);
    }

    /** @test */
    public function a_task_can_be_added_to_the_beginning_of_the_list()
    {
        $user = $this->create('App\User', [
            'email_verified_at' => Carbon::now(), 
        ]);
        $task1 = $this->create('App\Task', [
            'user_id' => $user->id,
            'position' => 1,
        ]);
        $task2 = $this->create('App\Task', [
            'user_id' => $user->id,
            'position' => 2,
        ]);

        $this->signIn($user);

        $this->post(route('tasks.store'), [
            'description' => 'Task description',
            'position' => 'first',
        ]);

        $this->assertDatabaseHas('tasks', [
            'user_id' => $user->id,
            'description' => 'Task description',
            'position' => 1,
        ]);
        $this->assertDatabaseHas('tasks', [
            'id' => $task1->id,
            'position' => 2,
        ]);
        $this->assertDatabaseHas('tasks', [
            'id' => $task2->id,
            'position' => 3,
        ]);
    }

    /** @test */
    public function a_task_can_be_added_to_the_end_of_the_list()
    {
        $user = $this->create('App\User', [
            'email_verified_at' => Carbon::now(),
        ]);
        $task1 = $this->create('App\Task', [
            'user_id' => $user->id,
            'position' => 1,
        ]);
        $task2 = $this->create('App\Task', [
            'user_id' => $user->id,
            'position' => 2,
        ]);

        $this->signIn($user);

        $this->post(route('tasks.store'), [
            'description' => 'Task description',
            'position' => 'last',
        ]);

        $this->assertDatabaseHas('tasks', [
            'user_id' => $user->id,
            'description' => 'Task description',
            'position' => 3,
        ]);
    }
}
