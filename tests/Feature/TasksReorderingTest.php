<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Task;

class TasksReorderingTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp()
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function tasks_can_be_reordered()
    {
        $task1 = $this->create('App\Task', ['user_id' => $this->user->id, 'position' => 1]);
        $task2 = $this->create('App\Task', ['user_id' => $this->user->id, 'position' => 2]);
        $task3 = $this->create('App\Task', ['user_id' => $this->user->id, 'position' => 3]);
        $task4 = $this->create('App\Task', ['user_id' => $this->user->id, 'position' => 4]);
        
        $this->assertEquals(
            [$task1->id, $task2->id, $task3->id, $task4->id],
            Task::orderBy('position')->pluck('id')->all()
        );
        
        $this->patch(route('tasks.reorder', [$task1, 2]));

        $this->assertEquals(
            [$task2->id, $task1->id, $task3->id, $task4->id], 
            Task::orderBy('position')->pluck('id')->all()
        );

        $this->patch(route('tasks.reorder', [$task3, 1]));

        $this->assertEquals(
            [$task3->id , $task2->id, $task1->id, $task4->id],
            Task::orderBy('position')->pluck('id')->all()
        );
    }

    /** @test */
    public function a_user_cannot_update_the_position_of_other_users_tasks()
    {
        $task1 = $this->create('App\Task', ['position' => 1]);
        $task2 = $this->create('App\Task', ['position' => 2]);
        $task3 = $this->create('App\Task', ['position' => 3]);

        $this->assertEquals(
            [$task1->id, $task2->id, $task3->id],
            Task::orderBy('position')->pluck('id')->all()
        );

        $this->patch(route('tasks.reorder', [$task3->id, 2]));

        $this->assertEquals(
            [$task1->id, $task2->id, $task3->id],
            Task::orderBy('position')->pluck('id')->all()
        );
    }
}
