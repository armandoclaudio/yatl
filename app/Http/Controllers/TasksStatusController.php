<?php

namespace App\Http\Controllers;

use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TasksStatusController extends Controller
{
    public function completed(Task $task)
    {
        if (request()->user()->id != $task->user_id) {
            return response('Unauthorized', 403);
        }

        $task->update([
            'completed_at' => Carbon::now(),
        ]);

        return $task->fresh();
    }

    public function incomplete(Task $task)
    {
        if (request()->user()->id != $task->user_id) {
            return response('Unauthorized', 403);
        }

        $task->update([
            'completed_at' => null,
        ]);

        return $task->fresh();
    }
}
