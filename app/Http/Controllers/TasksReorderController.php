<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TasksReorderController extends Controller
{
    public function update(Task $task, $newPosition)
    {
        if($task->user->id != request()->user()->id) {
            abort(403, 'Unauthorized'); 
        }

        request()->user()->tasks()
            ->where('id', '!=', $task->id)
            ->where('position', '>=', $task->position)
            ->where('position', '<=', $newPosition)
            ->decrement('position');
        
        request()->user()->tasks()
            ->where('id', '!=', $task->id)
            ->where('position', '<=', $task->position)
            ->where('position', '>=', $newPosition)
            ->increment('position');

        $task->update(['position' => $newPosition]);

        return response()->json([
            'success' => true
        ]);
    }
}
