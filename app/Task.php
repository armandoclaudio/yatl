<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $dates = ['completed_at', 'deleted_at'];
    protected $attributes = ['completed_at' => null];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
