<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::middleware('verified')->group(function () {
    Route::view('/', 'home.index')->name('home');
});

Route::middleware('auth.basic')->group(function () {
    Route::get('/tasks', 'TasksController@index')->name('tasks');
    Route::post('/tasks', 'TasksController@store')->name('tasks.store');
    Route::patch('/tasks/{task}', 'TasksController@update')->name('tasks.update');
    Route::delete('/tasks/{task}', 'TasksController@destroy')->name('tasks.destroy');
    Route::patch('/tasks/{task}/completed', 'TasksStatusController@completed')->name('tasks.status.completed');
    Route::patch('/tasks/{task}/incomplete', 'TasksStatusController@incomplete')->name('tasks.status.incomplete');
    Route::patch('/tasks/{task}/reorder/{newPosition}', 'TasksReorderController@update')->name('tasks.reorder');
});